CC=g++
CFLAGS=-c -Wall -Wl,--no-undefined -O0 -g -std=c++0x
LIBS=`pkg-config opencv cvblob --cflags --libs` `Magick++-config --cppflags --cxxflags --ldflags --libs`
LDFLAGS=$(LIBS) -Wall -O0 -g -std=c++11
SOURCES=$(shell find -name "*.cpp" | grep -v tests)
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=hw.cgi

.PHONY: clean cleanall doc

all: $(EXECUTABLE)
	cp $(EXECUTABLE) ../pages/cgi-bin/

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

%.o: %.cpp %.h
	$(CC) $(CFLAGS) $< -o $@

clean:
	-cd tests && make clean
	-rm $(OBJECTS)

cleanall: clean
	-cd tests && make cleanall
	-rm $(EXECUTABLE)

doc: $(SOURCES)
	doxygen Doxyfile

