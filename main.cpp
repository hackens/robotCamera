#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/photo/photo.hpp>
#include <opencv2/features2d/features2d.hpp>
#include "cvblob.h"

#define CRLF "\r\n"

using namespace cv;
using namespace std;
using namespace cvb;

Mat& filterHue(Mat& I, int min, int max, int minSat, int minValue) {
    // accept only char type matrices
    CV_Assert(I.depth() != sizeof(uchar));

    int channels = I.channels();

    int nRows = I.rows;
    int nCols = I.cols * channels;

    if (I.isContinuous()) {
       nCols *= nRows;
        nRows = 1;
    }

    int i,j;
    uchar* p;
    for( i = 0; i < nRows; ++i) {
        p = I.ptr<uchar>(i);
        for ( j = 0; j < nCols; ++j) {
			if (j%3 == 0)
	            p[j] = (p[j+1]<minSat || p[j+2]<minValue || p[j]<min || p[j]>max) ? 0 : 255;
            else
                p[j] = 0;
        }
    }
    return I;
}

Mat& keepComponent(Mat& I, int c) {
    // accept only char type matrices
    CV_Assert(I.depth() != sizeof(uchar));

    int channels = I.channels();

    int nRows = I.rows;
    int nCols = I.cols * channels;

    if (I.isContinuous()) {
       nCols *= nRows;
        nRows = 1;
    }

    int i,j;
    uchar* p;
    for( i = 0; i < nRows; ++i) {
        p = I.ptr<uchar>(i);
        for ( j = 0; j < nCols; ++j) {
			if (j%3 != c-1)
	            p[j] = 0;
        }
    }
    return I;
}


int main() {
  char* params = getenv("QUERY_STRING");

  VideoCapture cap(0); // open the video camera no. 0
  if (!cap.isOpened())  {
      cout << "Cannot open the video cam" << endl;
      return -1;
  }
  double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

  Mat frame;
for (int i=0; i<10; i++)
  cap.grab();
cap.read(frame);

    // Convert to HSV
    Mat hsv;
    cvtColor(frame, hsv, CV_BGR2HSV);
    blur(hsv,hsv,Size(2,2),Point(-1,-1));
    blur(frame,frame,Size(2,2),Point(-1,-1));

    // Filter hue
    Mat yellow = hsv.clone();
    filterHue(yellow,80,100,150,100);
    keepComponent(yellow,1);

    Mat binary(yellow.rows,yellow.cols, CV_8UC1, Scalar(255));
    const int fromTo[] = {0,0};
    mixChannels(&yellow, 1, &binary, 1, fromTo, 1);

    IplImage iBinary = binary.operator IplImage();
    IplImage *labelImg=cvCreateImage(cvGetSize(&iBinary), IPL_DEPTH_LABEL, 1);
    CvBlobs blobs;
    unsigned int result=cvLabel(&iBinary, labelImg, blobs);

    IplImage iFrame = frame.operator IplImage();
    cvRenderBlobs(labelImg, blobs, &iBinary, &iFrame);

  if (params !=NULL && string(params) == "robot") {

    CvBlob best;
    float bestArea=0;
    for (CvBlobs::const_iterator it=blobs.begin(); it!=blobs.end(); ++it) {
        float area = it->second->area;
        if (area>bestArea) {
            bestArea=area;
            best = *(it->second);
        }

    }
    cout<<best.area<<";"<<best.centroid.x<<";"<<best.centroid.y<<";"<<cvAngle(&best);
  }
  else if (params !=NULL && string(params) == "getframe") {
    std::cout << "Content-type: image/jpeg" << CRLF << CRLF;

    imwrite("image.jpg",yellow/*frame*/);

    FILE* f = fopen("image.jpg","rb");
    int c=0;
    while ((c=fgetc(f))!=EOF) {
      putchar((char)c);
    }
    fclose(f);
    cout<<endl;
  }
  else if (params !=NULL && string(params) == "ping") {
    cout << "Content-type:text/plain\r\n\r\n";
    Mat frameHSV;
    cvtColor(frame,frameHSV, CV_BGR2HSV);

    unsigned char *input = (unsigned char*)(frameHSV.data);
	long long int nPixels= 0;
    long long int totalValue = 0;
    for(int j = 0;j < frameHSV.rows;j++){
        for(int i = 0;i < frameHSV.cols;i++){
            unsigned char h = input[frameHSV.step * j + i ] ;
            unsigned char s = input[frameHSV.step * j + i + 1];
            unsigned char v = input[frameHSV.step * j + i + 2];
            nPixels++;
            totalValue+=v;
        }
    }

    std::cout<<"Value = "<<totalValue/nPixels<<std::endl;
std::cout<<" (value>90 means light is on)"<<endl;
  }
  else {
    cout << "Content-type:text/html\r\n\r\n";
    cout << "<html>\n";
    cout << "<head>\n";
    cout << "<title>Using this API</title>\n";
    cout << "</head>\n";
    cout << "<body>\n";
    cout << "<h1>Using this API</h1>"
      " <p><a href=\"?getframe\">Get a frame</a></p>"
      " <p><a href=\"?robot\">Robot info</a></p>"
      " <p><a href=\"?ping\">Get value (brightness)</a></p>";
    cout << "</body>\n";
    cout << "</html>\n"<<endl;
  }
  return 0;
}
